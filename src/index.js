import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import './game.css';
import Game from './Game';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import { initialGameState } from './constants';

import registerServiceWorker from './registerServiceWorker';

// ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<Cell id={22} bead={'BLUE_BEAD'}/>, document.getElementById('root'));
// ReactDOM.render(<Board redId={46}
//                        blueId={69}
//                        snakes={[{
//                            first: 10,
//                            second:20
//                        }]}
// />, document.getElementById('root'));
// ReactDOM.render(<Game redId={46}
//                        blueId={69}
// />, document.getElementById('root'));


import { rootReducer } from "./reducers";
import {rollDiceAction} from "./actions";

export const store = createStore(rootReducer);


{/*<Provider store={createStore(combinedReducer)}>*/}
ReactDOM.render(
    <Provider store={store}>
        <Game />
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();

// store.dispatch({
//     type: 'SET_OPPONENT_INFO',
//     opponentInfo: {
//         username: 'Ali 123',
//         country: 'Iran'
//     }
// });
//
// console.log(store.getState());
//
// store.dispatch({
//     type: 'RECEIVE_UPDATE',
//     pos: [100, 69], // pos[0] = blue
//     users: ['user123', 'opponent'],
//     turn: true
// });
//
// store.dispatch({
//     type: 'RECEIVE_UPDATE',
//     pos: [50, 90], // pos[0] = blue
//     users: ['user123', 'opponent'],
//     turn: true
// });
//
// store.dispatch(rollDiceAction());
//
//
//
// console.log(store.getState());