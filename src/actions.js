import {
    SET_OPPONENT_INFO, RECEIVE_UPDATE, ROLL_DICE, SNAKES, LADDERS, GAME_ONGOING, SET_MY_INDEX_ACTION,
    SET_USERS
} from "./constants";
import {Parse} from "./Game";

export const setMyIndexAction = (
    index
) => {
    return {
        type: SET_MY_INDEX_ACTION,
        myIndex: index
    };
};

export const isMyTurn = (
    result,
    myIndex,
    turn
) => {
    console.log('///isMyTurn');
    console.log(result);
    console.log(myIndex);
    console.log(turn);
    console.log('///END isMyTurn');
    if (myIndex === undefined || turn === undefined)
        return false;
    return (result === GAME_ONGOING &&
        (
            (myIndex === 0 && turn) ||
            (myIndex === 1 && !turn)
        )
    );
};


export const findNewPositionsAfterDice = (
    dice,
    pos,
    moveBlue
) => {
    // console.log('find New Pos after dice');
    // console.log(dice);
    // console.log(pos);
    // console.log(moveBlue);
    // console.log('find New Pos after dice');
    let movingIndex = 0;
    if (!moveBlue)
        movingIndex = 1;
    let retPos = pos;
    const newPos = pos[movingIndex] + dice;
    if (newPos > 100 || dice > 6 || dice < 1)
        return retPos;
    for (let i = 0; i < SNAKES.length; i++) {
        if (newPos === SNAKES[i][1]) {
            retPos[movingIndex] = SNAKES[i][0];
            return retPos;
        }
    }
    for (let i = 0; i < LADDERS.length; i++) {
        if (newPos === LADDERS[i][0]) {
            retPos[movingIndex] = LADDERS[i][1];
            return retPos;
        }
    }
    // console.log('hi3');
    retPos[movingIndex] = newPos;
    // console.log(retPos);
    return retPos;
};

const getDice = () => {
    const max = 6, min = 1;
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

// let Parse = require('parse');
// Parse.initialize('myAppId');
// Parse.serverURL = 'http://localhost:1337';

export const rollDiceAction = (diceRollState, dispatch) => {
    const { pos, myIndex, users, turn } = diceRollState;
    let dice = getDice();
    const retPos = findNewPositionsAfterDice(
        dice,
        pos,
        myIndex === 0 // then: i am blue -> move me. else i am red. moveBlue = false -> move red! :)
    );
    let Match = Parse.Object.extend("Match");
    let u0 = 'user123', u1 = 'user456';
    let query = Parse.Query.or(
        (new Parse.Query('Match')).equalTo("user0", users[myIndex]),
        (new Parse.Query('Match')).equalTo("user1", users[myIndex])
    );
    let toRet = {type: false};
    query.first({
        success: (match) => {
            if (dice !== 6)
                match.set('turn', !turn); //TODO
                // match.set('turn', !match.get('turn')); //TODO
            match.set('pos', retPos);

            console.log('in query first');
            match.save(null, {
                success: function(match) {
                    // Execute any logic that should take place after the object is saved.
                    console.log('in SUCCESS');
                    toRet = {
                        type: ROLL_DICE,
                        lastDice: dice
                    };
                    dispatch(toRet);
                    console.log(match);
                },
                error: function(match, error) {
                    // Execute any logic that should take place if the save fails.
                    // error is a Parse.Error with an error code and message.
                    console.log(match);
                    console.log(error);
                }

            });
            toRet = {
                type: ROLL_DICE,
                lastDice: dice
            }
            // match.save(null, {
            //     success: (match) => {
            //         console.log('at least success');
            //         console.log(match);
            //         toRet = {
            //             type: ROLL_DICE,
            //             lastDice: dice
            //         }
            //     },
            //     error: (match, error) => {
            //         console.log(match);
            //         console.log(error);
            //         console.log('errrroooorrrrr sssss');
            //     }
            //
            // });

            // match.save(null, {
            //     success: (match) => {
            //         console.log('at least success');
            //         console.log(match);
            //         toRet = {
            //             type: ROLL_DICE,
            //             lastDice: dice
            //         }
            //     },
            //     error: (match, error) => {
            //         console.log(match);
            //         console.log(error);
            //         console.log('errrroooorrrrr sssss');
            //     }
            //
            // });

        },
        error: (error) => {
            console.log('error in first!!');
        }
    });

    return toRet;
    // let match = new Match();
    // match.set('pos', retPos);
    // match.set('user0', users[0]);
    // match.set('user1', users[1]);
    // if (dice !== 6)
    //     match.set('turn', !turn);
    // else
    //     match.set('turn', turn);
    // match.save(null, {
    //     success: (match) => {
    //         console.log(match);
    //         //todo previous dice here: return
    //     },
    //     error: (match, error) => {
    //         console.log('error!!!!!!!');
    //         console.log(error);
    //     }
    // });
    // return {
    //     type: ROLL_DICE,
    //     dice
    // };
};

export const setOpponentInfoAction = (
    opponentInfo,
    users,
) => {
    return {
        type: SET_OPPONENT_INFO,
        opponentInfo,
        users
    };
};

export const setUsersAction = (
    users
) => {
    return {
        type: SET_USERS,
        users
    }
};

export const receiveUpdate = (
    update
) => {
    return {
        type: RECEIVE_UPDATE,
        ...update
    };
};
