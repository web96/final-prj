import {
    initialGameState, RECEIVE_UPDATE, ROLL_DICE, SNAKES, LADDERS, OPPONENT_IS_CONNECTED, GAME_ONGOING,
    SET_OPPONENT_INFO, SET_MY_INDEX_ACTION, SET_USERS
} from "./constants";

// export const findNewPositionsAfterDice = (
//     dice,
//     pos,
//     moveBlue
// ) => {
//     let movingIndex = 0;
//     if (!moveBlue)
//         movingIndex = 1;
//     let retPos = pos;
//     const newPos = pos[movingIndex] + dice;
//     if (newPos > 100 || dice > 6 || dice < 1)
//         return retPos;
//     for (let i = 0; i < SNAKES.length; i++) {
//         if (newPos === SNAKES[i][1]) {
//             retPos[movingIndex] = SNAKES[i][0];
//             return retPos;
//         }
//     }
//     for (let i = 0; i < LADDERS.length; i++) {
//         if (newPos === LADDERS[i][0]) {
//             retPos[movingIndex] = LADDERS[i][1];
//             return retPos;
//         }
//     }
//     // console.log('hi3');
//     retPos[movingIndex] = newPos;
//     // console.log(retPos);
//     return retPos;
// };
//user0 = blue pos0=blue
export const rootReducer = (state = initialGameState, action) => {
    console.log('action: ');
    console.log(action);
    if (!action.type)
        return state;
    switch (action.type) {
        case SET_USERS:
            if (!action.hasOwnProperty('users'))
                return state;
            let tempUsers = [state.myUsername, action.opponentUsername];
            if (state.myIndex)
            return {
                ...state,
                users: action.users
            };
        case ROLL_DICE:
            console.log('in reducer roll dice////');
            console.log(action);
            if (!action.hasOwnProperty('lastDice'))
                return state;
            console.log('in reducer roll dice22222');
            return {
                ...state,
                lastDice: action.lastDice
            };
        case SET_MY_INDEX_ACTION:
            if (action.myIndex !== 0 && action.myIndex !== 1)
                return state;
            return {
                ...state,
                myIndex: action.myIndex
            };
        case RECEIVE_UPDATE:
            console.log(action);
            const pos = action.pos,
                users = action.users;
            let myIndex;
            if (!state.hasOwnProperty('myIndex')){
                if (users[0] !== state.myUsername &&
                    users[1] !== state.myUsername)
                    return state;
                if (users[0] === state.myUsername)
                    myIndex = 0;
                else
                    myIndex = 1;
            }
            else
                myIndex = state.myIndex;
            // const myTurn = myIndex === 0 ?
            //     action.turn:
            //     !action.turn;
            if (!action.hasOwnProperty('turn'))
                return state;
            const turn = action.turn;
            return {
                ...state,
                pos,
                myIndex,
                turn
            };
        // case ROLL_DICE:
        //     const { retPos, dice } = findNewPositionsAfterDice(
        //         action.dice,
        //         state.pos,
        //         state.myIndex === 0 // then: i am blue -> move me. else i am red. moveBlue = false -> move red! :)
        //     );
        //     return {
        //         ...state,
        //         pos: retPos,
        //         myTurn: dice !== 6 ?
        //             !state.myTurn:
        //             state.myTurn
        //     };
        case SET_OPPONENT_INFO:
            return {
                ...state,
                opponentInfo: action.opponentInfo,
                users: action.users,
                result: GAME_ONGOING
            };
        // case OPPONENT_IS_CONNECTED:
        //     if (action.users[0] !== state.myUsername &&
        //         action.users[1] !== state.myUsername)
        //         return state;
        //     let myIndex = 0;
        //     if (action.users[1] === state.myUsername)
        //         myIndex = 1;
        //     return {
        //         ...state,
        //         // pos: [1, 1],
        //         // myTurn: action.myTurn,
        //         opponentInfo: action.opponentInfo,
        //         result: GAME_ONGOING
        //     };
        default:
            return state;
    }
};