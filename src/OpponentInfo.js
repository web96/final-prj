import React, { Component } from 'react';
import './game.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import {Row, PageHeader, small} from 'react-bootstrap'
import { connect } from 'react-redux';
import loading from "./img/please_wait.gif";

let OpponentInfo = ({
    opponentInfo
}) => {
    if (!opponentInfo || !opponentInfo.hasOwnProperty('username'))
        return (
            <Row>
                <img src={loading} id={"loading"}/>
            </Row>
        );
    return (
        <Row>
            <span>
                <PageHeader>
                    {'Opponent Information: '}
                </PageHeader>
                <PageHeader>
                    <small>{'Username: ' + opponentInfo.username}</small>
                </PageHeader>
                {opponentInfo.country ? // in case username: bot
                    <PageHeader>
                        <small>{'Country: ' + opponentInfo.country}</small>
                    </PageHeader> :
                    null
                }
            </span>
        </Row>
    );
};

const mapStateToProps = (state) => {
    return {
        opponentInfo: state.opponentInfo
    }
};

OpponentInfo = connect(mapStateToProps)(OpponentInfo);

export default OpponentInfo;