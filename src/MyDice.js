import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import './game.css';
import {Row} from 'react-bootstrap'
import one from './img/1.png';
import two from './img/2.png';
import three from './img/3.png';
import four from './img/4.png';
import five from './img/5.png';
import six from './img/6.png';
import { connect } from 'react-redux';
// import { isMyTurn } from "./actions";

const images = [one, two, three, four, five, six];

let MyDice = ({
    lastDice
}) => {
    if (!lastDice)
        return null;
    return (
        <Row id={'my-dice'}>
            <span>
                {'Previous Dice Number: '}
            </span>
            <img src={images[lastDice - 1]} className={'dice-image'}/>
        </Row>
    );
};

const mapStateToProps = (state) => {
    return {
        lastDice: state.lastDice
        // myTurn: isMyTurn(
        //     state.result,
        //     state.myIndex,
        //     state.turn
        // )
    }
};

MyDice = connect(mapStateToProps)(MyDice);

export default MyDice;