import React, { Component } from 'react';
import {Grid, Row, Col} from 'react-bootstrap'
import { connect } from 'react-redux';
import Board from './board-components/Board';
import RollDice from './RollDice';
import MyDice from './MyDice';
import OpponentInfo from './OpponentInfo';
import Result from './Result';
import {receiveUpdate, setMyIndexAction, setOpponentInfoAction, setUsersAction} from './actions';

export const Parse = require('parse');
Parse.initialize('myAppId');
Parse.serverURL = 'http://localhost:1337/parse';


let updateStatesFunc;
let myUsername;
let setMyIndex;
let setOpponentAndUsers;
class PresentationalGame extends Component {
    constructor(props) {
        super(props);
        // console.log('constructor updateStates');
        // console.log(this.props.updateStates);
        // this.updateStates = this.props.updateStates;
        updateStatesFunc = this.props.updateStates;
        myUsername = this.props.myUsername;
        setMyIndex = this.props.setMyIndex;
        setOpponentAndUsers = this.props.setOpponentAndUsers;
    }

    componentDidMount() {
        //let Match = Parse.Object.extend("Match");
        // let match = new Match();
        // let u0 = 'user123';
        // let u1 = 'user456';
        // match.set("pos", [1, 1]);
        // match.set("user0", u0);
        // match.set("user1", u1);
        // match.set("turn", true);
        // match.save(null, {
        //     success: function(match) {
        //         console.log(1111111111111);
        //         console.log(match);
        //     },
        //     error: function(match, error) {
        //         console.log(2222222222222);
        //         console.log(error);
        //     }
        // });
        let MatchRequest = Parse.Object.extend("MatchRequest");
        let matchRequest = new MatchRequest();
        matchRequest.set("user", myUsername);
        matchRequest.save();
        // let u0 = 'user123';
        //let u0 = myUsername;
        //console.log('myUSERNAME!!!!!!!!!!!!!!!!!!!!!!!!!');
        let u1 = 'user456';
        let query = Parse.Query.or(
            (new Parse.Query('Match')).equalTo("user0", myUsername),
            (new Parse.Query('Match')).equalTo("user1", myUsername)
        );
        let subscription = query.subscribe();
        subscription.on('create', (match) => {
            console.log('CREATED!!!!!');
            console.log(match);
            let users = [match.get('user0'), match.get('user1')];
            let opponentIndex;
            if (users[0] === myUsername) {
                setMyIndex(0);
                opponentIndex = 1;
            }
            if (users[1] === myUsername) {
                setMyIndex(1);
                opponentIndex = 0;
            }
            let opponentInfoQuery = new Parse.Query('User');
            opponentInfoQuery.equalTo("username", users[opponentIndex]);
            opponentInfoQuery.first({
                success: function (opponent) {
                    console.log("in opponent info success");
                    console.log(opponent);
                    setOpponentAndUsers({
                            username: opponent.get('username'),
                            country: opponent.get('country')
                        },
                        users
                    );
                },
                error: function (error) {
                    // alert("Error: " + error.code + " " + error.message);
                }
            });

            updateStatesFunc ({
                users: [match.get('user0'), match.get('user1')],
                pos: match.get('pos'),
                turn: match.get('turn')
            });
        });
        //let updateStates = this.updateStates;
        subscription.on('update', (match) =>{
            // console.log(updateStates);
            // console.log(match);
            // updateStates({
            updateStatesFunc ({
                users: [match.get('user0'), match.get('user1')],
                pos: match.get('pos'),
                turn: match.get('turn')
            });
        });
    }

    render() {
        // let props = this.props;
        return (
            <Grid id={'main-game-grid'}>
                <Row>
                    <Col xs={12} md={7}>
                        {/*<Board redId={props.redId} blueId={props.blueId} />*/}
                        <Board />
                    </Col>
                    <Col xs={12} md={3} mdOffset={1}>
                        {/*<RollDice myTurn={true}*/}
                                  {/*rollDiceClick={() => alert('hi')}*/}
                        {/*/>*/}
                        <RollDice />
                        {/*<MyDice myTurn={true}*/}
                                {/*myDice={5}*/}
                        {/*/>*/}
                        <MyDice />
                        {/*<OpponentInfo opponentInfo={{*/}
                            {/*username: 'just4fun',*/}
                            {/*country: 'Iran'*/}
                        {/*}}/>*/}
                        <OpponentInfo />
                    </Col>
                </Row>
                <Row>
                    <Col xs={6} mdOffset={3}>
                        {/*<Result result={'GAME_WON'}/>*/}
                        <Result />
                    </Col>
                </Row>
            </Grid>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ...state
    }
};

const mapDispatchToProps = (
    dispatch
) => {
    return {
        updateStates: (update) => {
            dispatch(receiveUpdate(update));
        },
        setMyIndex: (myIndex) => {
            dispatch(setMyIndexAction(myIndex));
        },
        setOpponentAndUsers: (opponentInfo, users) => {
            // dispatch(setUsersAction(opponentInfo.username, myIndex));
            // dispatch(setUsersAction(users));
            dispatch(setOpponentInfoAction(opponentInfo, users));
        }
    }
};

const Game = connect(mapStateToProps, mapDispatchToProps)(PresentationalGame);

export default Game;