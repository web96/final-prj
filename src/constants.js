// ACTION_TYPES
export const ROLL_DICE = 'ROLL_DICE';
export const SET_OPPONENT_INFO = 'SET_OPPONENT_INFO';
// export const OPPONENT_IS_CONNECTED = 'OPPONENT_IS_CONNECTED';
export const RECEIVE_UPDATE = 'RECEIVE_UPDATE';
export const SET_MY_INDEX_ACTION = 'SET_MY_INDEX_ACTION';
export const SET_USERS = 'SET_USERS';

// export const SEND_UPDATE = 'SEND_UPDATE';

export const SNAKES = [
    [6, 16],
    [11, 49],
    [19, 62],
    [25, 46],
    [53, 74],
    [60, 64],
    [68, 89],
    [75, 95],
    [80, 99],
    [88, 92],
];

export const LADDERS = [
    [2, 38],
    [7, 14],
    [8, 31],
    [15, 26],
    [21, 42],
    [28, 84],
    [36, 44],
    [51, 67],
    [71, 91],
    [78, 98],
    [87, 94],
];

export const NO_BEAD = 'NO_BEAD';
export const RED_BEAD = 'RED_BEAD';
export const BLUE_BEAD = 'BLUE_BEAD';
export const BOTH_BEAD = 'BOTH_BEAD';

export const GAME_ONGOING = 'GAME_ONGOING';
export const GAME_WAITING = 'GAME_WAITING';
export const GAME_WIN = 'GAME_WON';
export const GAME_LOST = 'GAME_LOST';


export const initialGameState = {
    result: GAME_WAITING,
    myUsername: 'user123',
    // myIndex: 0, // TODO DELETE THIS LINE
    // opponentInfo: {
    //     username: 'user456',
    //     country: 'USA'
    // },
    // pos: [1, 1],
    // users: ['user123', 'user456'],
    //turn: true
};

// export const initialGameState = {
//     result: GAME_ONGOING,
//     myUsername: 'user456',
//     opponentInfo: {
//         username: 'user123',
//         country: 'Iran'
//     },
//     pos: [1, 1],
//     users: ['user123', 'user456'],
//     myTurn: false
// };


// export const initialGameState = {
//     result: GAME_WAITING,
//     myUsername: 'user123'
// };

//
// export const initialGameState = {
//     //isBlue: true,
//     pos: [72, 10],
//     myTurn: false,
//     lastDice: 6,
//     opponentInfo: {
//         username: 'Ali',
//         country: 'Iran'
//     },
//     result: GAME_ONGOING
// };
