import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import './board-components.css';
import Bead from './Bead';
import { NO_BEAD, RED_BEAD, BOTH_BEAD } from "../constants";

//TODO Cell props.color cell background-color or background-image
//const Cell = (props) => {
class Cell extends Component{
    render() {
        let props = this.props;
        const bead =
            props.bead === NO_BEAD ?
                null :
                (
                    props.bead === RED_BEAD ?
                        'red-bead' :
                        'blue-bead'
                );
        const bead2 =
            props.bead === BOTH_BEAD ?
                'red-bead':
                null;
        return (
            <div className={'cell'}>
                {bead !== null ?
                    <Bead bead={bead}/> :
                    null
                }
                {bead2 !== null ?
                    <Bead bead={bead2}/> :
                    null
                }
            </div>
        );
    }
}

export default Cell;
