import React, { Component } from 'react';
import './board-components.css';

const Bead = (props) => (
    <div className={'bead ' + props.bead}>
    </div>
);

export default Bead;