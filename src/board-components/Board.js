import React, { Component } from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css'
import './board-components.css';
import {NO_BEAD, RED_BEAD, BLUE_BEAD, BOTH_BEAD} from "../constants";
import Cell from './Cell';
import {Grid, Row} from 'react-bootstrap'
import { connect } from 'react-redux';


class PresentationalBoard extends Component {
    constructor(props) {
        super(props);
        this.nrows = 10;
        this.ncols = 10;
        // this.cellComponents = new Array(this.nrows * this.ncols);
    }

    render() {
        const props = this.props;
        const cells = new Array(this.nrows).fill(undefined).map((_, rowIndex) =>
            new Array(this.ncols).fill(undefined).map((_, colIndex) =>
                this.ncols * rowIndex + colIndex + 1)).reverse();
        cells.map((row, index) => index % 2 === 0 ? row.reverse() : row);
        //todo IE fill
        let redId = props.redId;
        let blueId = props.blueId;
        return (
        //let temp =
            <Grid id={'grid-board'}>
                {cells.map(
                    (row, rowIndex) => {
                        return (
                            <Row key={rowIndex}
                                 id={'grid-row'}
                                 className={'show-grid'}
                            >
                                {
                                    row.map(
                                        (cellId, colIndex) => {
                                            return (
                                                <Cell key={colIndex}
                                                      id={cellId}
                                                      bead={
                                                          cellId === redId && cellId === blueId ?
                                                              BOTH_BEAD :
                                                              cellId === blueId ?
                                                                  BLUE_BEAD :
                                                                  cellId === redId ?
                                                                      RED_BEAD : NO_BEAD
                                                      }
                                                />

                                            );
                                        }
                                    )
                                }
                            </Row>
                        );
                    }
                )}
            </Grid>
        );
    }
}


//
// <Cell key={colIndex}
//       id={cellId}
//       bead={
//           cellId === redId ?
//               RED_BEAD :
//               cellId === blueId ?
//                   BLUE_BEAD : NO_BEAD
//       }
// />

const mapStateToProps = (state) => {
    return {
        //redId: state.pos,
        // pos[0]: blue pos[1]: red
        blueId: state.hasOwnProperty('pos') ? state.pos[0] : -1,
        redId: state.hasOwnProperty('pos') ? state.pos[1] : -1,
    }
};

const Board = connect(mapStateToProps)(PresentationalBoard);

export default Board;