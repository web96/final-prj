import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import {Row, Button} from 'react-bootstrap'
import { connect } from 'react-redux';
import { GAME_ONGOING } from './constants';
import { rollDiceAction, isMyTurn } from './actions';
import {store} from "./index";


let RollDice = ({
    disable,
    rollDiceClick
}) => {
    return (
        <Row id={'roll-dice'}>
            <Button bsStyle={'primary'}
                    bsSize={'large'}
                    disabled={disable}
                    onClick={rollDiceClick}
            >
                Roll The Dice
            </Button>
        </Row>
    );
};

const mapStateToProps = (state) => {
    return {
        disable: !isMyTurn(
            state.result,
            state.myIndex,
            state.turn
        ),
        pos: state.pos,
        users: state.users,
        turn: state.turn,
        myIndex: state.myIndex
    };
};

const mapDispatchToProps = (
    dispatch,
    ownProps
) => {
    return {
        rollDiceClick: () => {
            // dispatch(
            console.log('/// mapdispatch roll dice:');
            console.log(ownProps);
            console.log('///END mapdispatch roll dice:');
            // let temp = rollDiceAction(store.getState());
            console.log('!!!!!!in DISPATCH!!!!!!!');
            console.log(store.getState());
            rollDiceAction(store.getState(), dispatch);
            // console.log(temp);
            // dispatch(
            //     temp
            // );
            // dispatch(
            //     rollDiceAction(store.getState())
            // );


            // dispatch(
            //     rollDiceAction(
            //         ...ownProps
            //     ));
        }
    }
};

RollDice = connect(mapStateToProps, mapDispatchToProps)(RollDice);

export default RollDice;