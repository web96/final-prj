import React, { Component } from 'react';
import './game.css';
import {GAME_WIN, GAME_WAITING, GAME_ONGOING} from "./constants";
import {Row} from 'react-bootstrap'
import win from './img/win.gif';
import lose from './img/lose.gif';
import { connect } from 'react-redux';

let Result = ({
    result
}) => {
    if (result === GAME_ONGOING || result === GAME_WAITING)
        return null;
    return (
        <Row>
            <img className={'result-image'} src={result === GAME_WIN ? win : lose} />
        </Row>
    );
};

const mapStateToProps = (state) => {
    return {
        result: state.result
    };
};

Result = connect(mapStateToProps)(Result);

export default Result;
